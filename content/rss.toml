title = "RSS: the Old New Way of the Internet"
summary = """A quick guide to bringing the internet to you"""
content = """
Alright alright here we go. RSS. Really Simple Syndication.

![header image of mt. adams](./assets/adams.jpg)

The first notions of RSS date back to 1995 in Apple's Advanced Technology Group, with the first version spawning from Netscape. The short long is that RSS is a way to subscribe to websites or sub-sections of websites -- like Twitter for the internet at large. You can subscribe to your favorite youtuber, get updates on your favorite instagram tag, have the latest news from your favorite publisher beamed to your device when the big story breaks, get your fix from your favorite podcaster, etc. all through this way of structuring the internet.

My personal stake in this is that it can be a potent method of bypassing very bad or nigh on user-hostile sites or designs. Or ads. I believe it embodies what spirit of the internet is really about: distribution of information.

A couple years back, there was an effort to posture in such a way that made it seem like the standard had died, but many technologists would opine that it never really did -- myself included. Recently, RSS has seen a resurgence of interest, and I would like to dogpile on that. RSS is oddx approved tech, and you should use it.

There have been a few formats to split off, atom being one of them. Atom is super similar to RSS, and some RSS readers parse atom as well, though I'm not sure if all do. Which is the correct format is up for debate, but given that atom was not so superior that it could majorly dethrone RSS tells me that RSS is still the champion format.

#### So How Does it Work?

Basically, RSS is an xml file with a bunch of links, associated titles and summaries, and post dates that a reader parses through and presents to you in a human-friendly way. This is actually very similar to  how a basic web browser works! You tap on one of the items in your feed, and if the summary wasn't enough delicious information for you, you can then tap on the link, where the reader will take you directly to the page on the site that contains the full content. For my favorite comic websites, the comic itself is usually displayed in the summary, so I typically don't have to ever actually visit the site. If you happen upon this case, congrats, you've successfully brought the internet to you! (I'm currently contemplating how best to make this happen for my blog -- stay tuned!) Tragically, site that are this generous with their content are usually the ones you want to support by giving them ad revenue by visiting their site, but for the moral argument, there are other means of supporting your favorite content creators. I imagine this case also happens with youtube channels and instagram tags.

A large swath of sites support RSS and if they do, if they *get it*, they'll have an icon that looks like this:

![rss icon](http://icons.iconarchive.com/icons/danleech/simple/1024/rss-icon.png#square)

Might be different colors, though. You can click on that, nab the link it redirects you to, and throw that in your favorite RSS Reader. Refresh that bad boy, and you should see the most recent, or maybe all, posts show up in your feed.

A useful tip for sniffing out feeds is that, since you're ultimately looking for an xml file adhering to the rss spec and that file is typically situated at the root of the website, you can navigate to your favorite site and append "/rss.xml" to the url. This is not a surefire way to find a feed, though, as I've definitely seen sites have a node dedicated to their feed, so it would look like https://rss.awesome.com, which, from a url perspective looks dope, but I think strays a bit from the friendly original form.

Bigger tech companies may not have first party support, but there are work arounds for this. Twitter, for example, does not seem to natively support RSS, but one could use a third party service like [twitrss.me](https://twitrss.me) to generate a feed. (At the time of writing, twitrss's ssl certificates have expired, and their service is down -- perhaps when you read this, it will be back.) I don't necessarily recommend third party services since RSS is kinda meant to cut out middle men, but I have used them before.

There are similar ways to turn non-rss-compliant websites into feeds, but I don't use them too often.

Another protip before I move on: generally, you don't want to make accounts for rss stuff since, again, rss is meant to cut down on profiling. There are exceptions, though. Like, if you want to have a reader that syncs in your browser and your mobile device, you'll probably end up needing an account with some reader service. In that case, I would recommend a paid service with a privacy statement, so you know they at least aren't incentivized to sell your data.

As for youtube, there's this very helpful youtube video (how meta!) on how to find a feed for a channel or playlist:

<iframe class="youtube" width="1228" height="480" src="https://www.youtube.com/embed/WmbPhkW8PHQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The gist is that for a channel, you want to nab whatever hash follows "/channel/" in the channel url (should be a fairly long string of letters and numbers), and throw that after "https://www.youtube.com/feeds/videos.xml?channel_id=". For a youtube playlist, do the same thing, but throw the hash after "https://www.youtube.com/feeds/videos.xml?playlist_id=".

Last thing of note: many readers support an export function, somtimes in a .opml format, so that you can push around your nice little curated slice of the internet. One could take this and throw it up in the cloud on something like github so it's version controlled and lasts forever.

#### What Reader Should I Use?

Ideally, a free one that doesn't make you create a profile, but use whatever you like. On iPhone I use [RSS Mobile](https://apps.apple.com/us/app/rss-mobile/id533007246), and I don't use a desktop or browser reader. I hear good things about the one-time fee apps. Check the app size -- these things really shouldn't be much over 10MB.

On RSS Mobile, you can add a feed by tapping the plus sign next to "FEEDS" in the nav menu. If you would like to subscribe to this blog, put "https://blog.oddx.xyz" in the url bar that comes up. This app is kinda nifty because it will search for the feed itself. Tap "VIEW" when the site comes up. It should default to selecting the feed on this blog. Hit "Done". The "Transmute" should show up under "FEEDS". Tap on that (or "All Posts" up at the top of the nav), and bam. You just subscribed to the internet.

<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/476064279&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/iamlpx" title="LPX" target="_blank" style="color: #cccccc; text-decoration: none;">LPX</a> · <a href="https://soundcloud.com/iamlpx/might-not-make-it-home" title="Might Not Make it Home" target="_blank" style="color: #cccccc; text-decoration: none;">Might Not Make it Home</a></div>

Go buck wild.
"""

[taxonomies]
tags = ["Tech"]
